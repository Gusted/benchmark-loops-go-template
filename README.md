# Benchmark go templates

Benchmark loop operations in Go templates. Native vs templates.

```
go test -bench .
```
