package main

import (
	"html/template"
	"io"
	"log"
	"testing"
)

var guardians = []string{"Gamora", "Groot", "Nebula", "Rocket", "Star-Lord"}

func BenchmarkLoopNative(t *testing.B) {
	mainTempl := `Has Rocket in slice: {{HasInSlice . "Rocket"}}`
	funcs := template.FuncMap{"HasInSlice": func(input []string, needle string) template.HTML {
		for _, maybeNeedle := range input {
			if maybeNeedle == needle {
				return template.HTML("true")
			}
		}
		return template.HTML("false")
	}}

	parsedTmpl, err := template.New("main").Funcs(funcs).Parse(mainTempl)
	if err != nil {
		log.Fatal(err)
	}

	for i := 0; i < t.N; i++ {
		if err := parsedTmpl.Execute(io.Discard, guardians); err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkLoopTempl(t *testing.B) {
	mainTempl := `Has Rocket in slice: {{range .}}{{if eq . "Rocket"}}true{{end}}{{end}}`

	parsedTmpl, err := template.New("main").Parse(mainTempl)
	if err != nil {
		log.Fatal(err)
	}

	for i := 0; i < t.N; i++ {
		if err := parsedTmpl.Execute(io.Discard, guardians); err != nil {
			log.Fatal(err)
		}
	}
}
